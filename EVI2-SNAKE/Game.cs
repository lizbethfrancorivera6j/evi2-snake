﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Threading;

namespace EVI2_SNAKE
{
     class Program
    {
        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            Console.SetWindowSize(40, 20);

            Game game = new Game();
            game.Run();
        }
    }

    public class Game
    {
        private List<Point> snake;
        private Point food;
        private int score;
        private Direction direction;
        private bool isGameOver;

        public Game()
        {
            snake = new List<Point>
            {
                new Point(10, 10),
                new Point(10, 11),
                new Point(10, 12)
            };

            food = new Point();
            GenerateFood();

            score = 0;
            direction = Direction.Right;
            isGameOver = false;
        }

        public void Run()
        {
            while (!isGameOver)
            {
                Draw();
                Thread.Sleep(100); // Controla la velocidad del juego

                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    ChangeDirection(key);
                }

                Move();
                CheckCollision();
            }

            Console.SetCursorPosition(0, 20);
            Console.WriteLine("Game Over! Your score: " + score);
        }

        private void Draw()
        {
            Console.Clear();

            // Dibuja la serpiente
            foreach (Point point in snake)
            {
                Console.SetCursorPosition(point.X, point.Y);
                Console.Write("*");
            }

            // Dibuja la comida
            Console.SetCursorPosition(food.X, food.Y);
            Console.Write("@");

            // Muestra el puntaje
            Console.SetCursorPosition(0, 20);
            Console.WriteLine("Score: " + score);
        }

        private void Move()
        {
            // Mueve la serpiente
            Point head = snake[0];
            Point newHead = new Point(head.X, head.Y);

            switch (direction)
            {
                case Direction.Up:
                    newHead.Y--;
                    break;
                case Direction.Down:
                    newHead.Y++;
                    break;
                case Direction.Left:
                    newHead.X--;
                    break;
                case Direction.Right:
                    newHead.X++;
                    break;
            }

            snake.Insert(0, newHead);

            // Verifica si la serpiente come la comida
            if (newHead.X == food.X && newHead.Y == food.Y)
            {
                score++;
                GenerateFood();
            }
            else
            {
                snake.RemoveAt(snake.Count - 1); // Elimina la cola de la serpiente
            }
        }

        private void GenerateFood()
        {
            Random random = new Random();
            bool isFoodGenerated = false;

            while (!isFoodGenerated)
            {
                food.X = random.Next(0, 40); // Ajusta según el tamaño de la ventana
                food.Y = random.Next(0, 20); // Ajusta según el tamaño de la ventana

                // Verifica que la comida no aparezca sobre la serpiente
                bool overlap = false;
                foreach (Point point in snake)
                {
                    if (food.X == point.X && food.Y == point.Y)
                    {
                        overlap = true;
                        break;
                    }
                }

                if (!overlap)
                {
                    isFoodGenerated = true;
                }
            }
        }

        private void ChangeDirection(ConsoleKeyInfo key)
        {
            switch (key.Key)
            {
                case ConsoleKey.UpArrow:
                    if (direction != Direction.Down)
                        direction = Direction.Up;
                    break;
                case ConsoleKey.DownArrow:
                    if (direction != Direction.Up)
                        direction = Direction.Down;
                    break;
                case ConsoleKey.LeftArrow:
                    if (direction != Direction.Right)
                        direction = Direction.Left;
                    break;
                case ConsoleKey.RightArrow:
                    if (direction != Direction.Left)
                        direction = Direction.Right;
                    break;
            }
        }

        private void CheckCollision()
        {
            // Verifica la colisión con las paredes
            Point head = snake[0];

            if (head.X < 0 || head.X >= 40 || head.Y < 0 || head.Y >= 20) // Ajusta según el tamaño de la ventana
            {
                isGameOver = true;
                return;
            }

            // Verifica la colisión con la propia serpiente
            for (int i = 1; i < snake.Count; i++)
            {
                if (head.X == snake[i].X && head.Y == snake[i].Y)
                {
                    isGameOver = true;
                    return;
                }
            }
        }
    }

    public struct Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }

    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }
}
    
